# Raspberry Pi Documentation

This folder contains documentation about the Raspberry Pi 3 that is installed in both the static and mobile camera.

## SSH Configuration

The camera code uses SSH to upload images to `nps-cams.wpi.edu`.  A recent change to `ssh-keygen` means keys are now by default saved in the new `RFC4716` format, instead of the traditional `PEM`.  Unfortunately the libraries used by the code do not yet support this key format.  Instead you must generate the private key with `ssh-keygen -m PEM -t rsa -b 4096`.

## Packages

The following packages are required

`python3-pip`

`ttf-mscorefonts-installer`

The following python packages are required

`pysftp`

`picamera`

There might be additional apt or pip3 packages that you need to install, I might have forgotten to write a couple down when getting things set up.
