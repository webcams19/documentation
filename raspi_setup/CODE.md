# Camera Code Documentation

Here's some documentation for the camera code.

## Configuration

Here's an overview of the various configuration options and what they're for

| Value | Description |
|-------|-------------|
| `ec25.enable_gps` | Enables or Disables the GNSS unit of the EC25 |
| `camera.resolution` | Determines the capture resolution |
| `power.disconnect_after_upload` | Disable the LTE unit when upload is complete |
| `power.disconnect_at_night` | Disable the LTE unit when the camera is idling at night |
| `interval.start_time` | Time of day in minutes to start taking pictures |
| `interval.stop_time` | Time of day in minutes to stop taking pictures |
| `interval.interval` | Time to wait in minutes between pictures |
| `server.username` | Username to use when uploading pictures |
| `server.key_file` | Private key to use when uploading pictures |
| `server.hostname` | Server to upload to |
| `server.directory` | Directory to upload images to on the remote server |
| `server.retry_count` | Number of times to retry a failed upload |
| `server.retry_delay` | Time in seconds (times retry counter) to wait before retrying failed upload |
| `capture.name_full` | Name to embed in the captured image's annotation area |
| `capture.name_base` | Base name to prepend to the image file name |
| `capture.park_name` | Park name to embed in the captured image's annotation area |
| `capture.quality` | JPEG quality to save captured and annotated images at
