# How to configure the GPS and GPSD

## Prereqs

`gpsd`

### From `pip3`
`pyserial`

## Setting things up

1. Kill the `gpsd` service with `sudo service gpsd stop`
2. Disable the `gpsd` service with `sudo systemctl disable gpsd`
3. Create a script that performs the following actions
    0. Kill existing `gpsd` sessions with `killall gpsd`
    1. Opens a serial connection to `/dev/ttyUSB2`
    2. Executes `AT+QGPSEND` to kill ghost sessions
    3. Executes `AT+QGPS=1` to start GPS
    4. Initializes `gpsd` with `gpsd /dev/ttyUSB1`
4. Configure that script to be executed whenever you need GPS
