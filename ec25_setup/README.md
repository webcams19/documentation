# EC25 Documentation

This folder contains documentation about the EC25 LTE/GPS module that is installed in both the static and mobile cameras.  Here you will find manuals for the device, as well as guides for getting it configured.
